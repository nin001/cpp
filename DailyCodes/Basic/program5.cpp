// fun code

#include <iostream>

void fun() {
	std::cout << "In fun" <<std::endl;
}

/*** In c waring of implicit declaration comes
 * but in cpp , error of method not declared in the
 * scope occurs , if we do not write method declaration
 */
