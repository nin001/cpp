// Destructor call in inheritance
//
// First child destructor is called and then parents
//
// Reason:
// 	-As parent cannot access childs members , but child
// 	can , if first parents object is freed than , child cannot
// 	access the members (instance variables) as Parent's object
// 	is freed
// 	-Child's scope is over and parents scope is within child
// 	so first parent is freed and then child 

#include <iostream>

class Parent {
public:
	Parent() {
		std::cout << "Parent constructor" << std::endl;
	}
	~Parent() {
		std::cout << "Parent Destructor" << std::endl;
	}
};

class Child : public Parent {
	public:
		Child() {
			std::cout << "Child constructor" << std::endl;
		}
		~Child() {
			std::cout << "Child destructor" << std::endl;
		}
};

int main() {

	Child obj;
	return 0;
}
