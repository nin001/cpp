// Constructor call in inheritance
//
// If there exist parent-child relation in classes
// and if we create derived classes object , then 
// parents constructor is called in the child constructor
// so that Parents instance variables are initialized and
// ready to access by Child class

#include <iostream>

class Parent {
	public:
		Parent() {
			std::cout << "Parent constructor" << std::endl;
		}
};

class Child : public Parent{
	public:
		Child() {
			std::cout << "Child constructor" << std::endl;
		}
};

int main() {
	
	Child obj;
	return 0;
}
