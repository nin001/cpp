// Composition
//
// Before Inheritance , Concept of composition was used
// that is creating object of a class inside another class
// with which we can access the classes members
//
// Using composition , it would be complicated to initialize 
// instance variables of class whose object is inside the class

#include <iostream>

class Employee {
	std::string eName = "Niraj";
	int empId = 101;

	public:
		void getData() {
			std::cout << eName << " " << empId << std::endl;
		}
};

class Company {
	std::string cname = "Veritas";
	int eCount = 1500;
	Employee obj;

	public:
	Company(std::string cname , int eCount) {
		this->cname = cname;
		this->eCount = eCount;
	}

	void getInfo() {
		std::cout << cname << " " << eCount << std::endl;
		obj.getData();
	}
};

int main() {
	Company obj("Pubmatic",5000);

	obj.getInfo();
	return 0;
}
