// Winline 
// Throws warning if the function is actually turned inline or not

#include<iostream>

inline int add(int x , int y) {
	return x+y;
}

int main() {
	printf("%d\n",add(10,20));
	int (*funPtr)(int,int) = add;
	printf("%p\n",funPtr);
	
}
