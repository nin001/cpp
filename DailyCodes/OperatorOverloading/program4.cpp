// Using member function 
// As member function gets a hidden parameter this , 
//
// Member functions can be used only in overloading those operators whose
// First parameter is object , If first parameter is not object then member
// functions cannot be used to overload the operator as the first parameter 
// of the function is this pointer which is the object's pointer

#include <iostream>

class Demo {
	int x = 10;
	public:
	Demo(int x) {
		this->x = x;
		std::cout << "in constructor" << std::endl;
	}

	// member function for insertion operator '<<'
	std::ostream& operator<<(std::ostream& cout) {
		std::cout << this->x ;
		return cout;
	}
	/*Explaination
	 * As first parameter of the member function is hidden this pointer , instead
	 of std::cout << obj , we can write obj<<std::cout;
	 which will take first parameter as an object which will match the hidden this pointer
	 and while overloading the operator<< , just giving it one ostream reference parameter which
	 is cout */
};

int main() {
	
	Demo obj(100);

	obj << std::cout << std::endl;
	return 0;
}
