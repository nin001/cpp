// static variables and static member functions in class

#include <iostream>

class Demo {

	public:
	int x = 10;   //instance variable or non-static variable
	//static init ways 
	//1. decalre here and initialize in global namespace with demo ::
	static int y1;

	//2. making static variable constant
	static const int y2 = 20;

	//3. Using inline keyword for static init
	static inline int y3 = 30;

};


int Demo::y1 = 50;

int main() {

	Demo obj;
	std::cout << obj.x << std::endl;

	
	std::cout << obj.y1 << std::endl;
	std::cout << obj.y2 << std::endl;
	std::cout << obj.y3 << std::endl;
	
	return 0;
}
/*
program7.cpp:10:20: error: ISO C++ forbids in-class initialization of non-const static member ‘Demo::y’
   10 |         static int y = 10;
         |*/
