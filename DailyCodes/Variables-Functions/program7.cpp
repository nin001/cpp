// Reference variables internals

#include <iostream>

int main() {
	int x = 10;
	
	int &y = x;

	//	^compiler adds & (address) before the x variable and removes & before y and 
	//	makes is a constant pointer
	//	like this
	//	int *const y = &x;
	//
	//	Internally , Reference variable is a pointer which cannot reference to other
	//	varible but only to the one who have been initialized with
	

	std::cout << x << std::endl;		//10
	std::cout << y << std::endl;		//10
	std::cout << &y << std::endl;		//100
	std::cout << &y << std::endl;		//100

	/*
	 * Compiler when sees a reference variable , 
	 * it adds * (dereferencing operator) before every time reference variable
	 * is invoked
	 *
	 * compiler implicitly derefences the variable while calling hence , 
	 * when done 
	 * y  --> internally it goes *y which represents value at x(address stored in y)
	 *
	 * &y --> &(*y) 
	 * 	^ this means that it goes to the data in the x box
	 * 	and again prints address 
	 * 	this is the reason that the address of x and y be same
	 */

}
