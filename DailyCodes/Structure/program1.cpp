// Structure in cpp are same as structure in cpp but with some 
// additional features
// Features : 
// 1. We can initialize value inside a structure that was not allowed in c
// 2. We can define functions inside a structure that can be called by creating object of 
// that structure
// 3. We can Create Object of structure by just the structure name :
// eg - Let a structure be of Player 
// object creation in c : struct Player obj;
// object creation in cpp : Player obj;
// 				^ struct keyword not required for creating object
// 				cpp consider structure as a data type

#include <iostream>

struct Player {
	char name[20];
	int jrNumber;

	void display() {
		std::cout << "Name : " << name << std::endl;
		std::cout << "Jersy Number : " << jrNumber << std::endl;
	}

	void insert() {

		std::cout << "Enter Player Name : " << std::endl;
		std::cin.getline(name,20);

		std::cout << "Enter Jersy Number : " << std::endl;
		std::cin >> jrNumber;
	}
};

int main() {
	Player obj;
	obj.insert();
	obj.display();
	return 0;
}
