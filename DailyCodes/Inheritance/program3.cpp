// Inheritance in cpp
//
// Inheritance is a concept where , we create a class which generally 
// contains member functions and vairables which are common 
// and then we make that class parent of a class , and that child class
// can access the members of the parent
//
// Parent -> Base class
// Child -> Derived class
// 
// Types of Inheritance in CPP
// - public
// - private
// - protected
//
// Forms of Inheritance
// - Single
// - Multiple
// - Multilevel
// - Heirarchical
// - Hybrid

#include <iostream>

class Parent {
	int x = 10;
	protected:
	int y = 20;
	public:
	int z = 30;
};

class Child : Parent {
	public:
		void getInfo() {
			std::cout << y << z << std::endl;
		}
};

int main() {
	Child obj;

	obj.getInfo();
	return 0;
}
