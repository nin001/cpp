// function calls 
//
// matching arguments
//
// First exact match is searched and if found , 
// invokation of matching function is done , and if
// no matching function found , then molded and goes
// for other function

#include <iostream>

void fun(int x ,int y) {
	std::cout << "int - int" << std::endl;
}

void fun(int x , float y) {
	std::cout << "int-float" << std::endl;

}

void fun(float x , int y) {
	std::cout << "float-int" << std::endl;
}

void fun(float x , float y) {
	std::cout << "float - float" << std::endl;
}

void fun(double x , double y) {
	std::cout << "double-double" << std::endl;
}

int main() {

	fun(10,20);

	fun(10.5f , 30);	// if only int-int and float-float is available , then this call is ambigious
	
	fun(1.5 , 2.5);	// is double-double , but can be upgraded to int and float , but choices should not be ambigious
	
	

	return 0;
}
