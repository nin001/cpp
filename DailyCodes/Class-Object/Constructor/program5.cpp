// call to copy constructor after creating array of objects
// Demo arr[] = {obj1,obj2..};
//
// here internal calls are
// arr[0] = obj1 . arr[1] = obj2;
// which calls copy constructor

#include <iostream>

class Demo {
	public:
		Demo() {
			std::cout << "no-args" << std::endl;
		}

		Demo (Demo& ref) {
			std::cout << "copy" << std::endl;
		}
};

int main() {
	Demo obj1;
	Demo obj2;
	Demo obj3;

	Demo arr[] = {obj1,obj2,obj3};

	return 0;
}
