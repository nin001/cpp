// Classes 
// Class is a speacial structure which can store different data under single name
// we can create a class object using new which creates object in heap section
// and using just class name which creates object inside stack frame

#include <iostream>

class Player {
	int x;
	int y;

	void insert() {
		std::cout << "Enter number : " << std::endl;
		std::cin >>x;
		std::cout << "Enter number : " << std::endl;
		std::cin >>y;
	}
};

int main() {
	Player obj;

//	obj.insert();
	//private member function is not accessible outside class
/*program1.cpp: In function ‘int main()’:
program1.cpp:23:19: error: ‘void Player::insert()’ is private within this context
   23 |         obj.insert();
      |         ~~~~~~~~~~^~
program1.cpp:12:14: note: declared private here
   12 |         void insert() {
      |              ^~~~~~
*/
}

/*
 * Everything inside class which is not written under any block is private by default
 * private block : The variables and member functions is not accessible outside class but 
 * can be accessed by member functions;
 */
