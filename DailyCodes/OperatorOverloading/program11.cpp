// Overloading operator / using member function

#include <iostream>

class Demo {
	int x = 10;
	public:
	Demo(int x) {
		this->x = x;
		std::cout << "in constructor" <<std::endl;
	}

	int operator/(Demo& obj) {
		return this->x / obj.x;
	}

};

int main() {

	Demo obj1(120);
	Demo obj2(30);

	std::cout << obj1/obj2 << std::endl;

	return 0;
}
