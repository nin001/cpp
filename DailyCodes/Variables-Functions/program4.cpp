// Functions 
// Name mangling of functions
// We can write functions of same name with different types and set of parameters
// During compile time compiler changes the functions names and makes them unique 
// compiler also matches respective function calls name and avoid confusion
//
// How compiler changes the names
// eg.
// fun1 name - add(int,int) -->> addii(int ,int)
// fun2 name - add(float,int) -->> addfi(float,int)
//
// Compiler changes the names according to the processor types

#include<iostream>

int add(int x , int y){
	return x+y;
}
/*
int add(float x , float y){
	return x+y;
}

int add(int x , float y){
	return x+y;
}

int add(float x , int y){
	return x+y;
}
*/
int main() {
	std::cout << add(10,20) << std::endl;
	std::cout << add(10.5f,30) << std::endl;
	std::cout << add(10,20.5f) << std::endl;
	std::cout << add(10.5f,20.5f) << std::endl;
//	std::cout << add(10.5,20.5) << std::endl;    //error for this line
}

/*
program4.cpp:37:25: error: call of overloaded ‘add(double, double)’ is ambiguous
   37 |         std::cout << add(10.5,20.5) << std::endl;
      |                      ~~~^~~~~~~~~~~
Reason : 
	There is no perfect match of parameters and arguments in functions then it can 
	go to any of the functions which makes it ambiguous
*/
