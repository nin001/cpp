// overloading extraction operator using normal function

#include <iostream>

class Demo {
	int x;
	public:
	void setData(int x) {
		this->x = x;
	}

	void getData() {
		std::cout << x << std::endl;
	}
};

std::istream& operator>>(std::istream& in , Demo& obj) {
	int x;
	in >> x;
	obj.setData(x);
	return in;
}

int main() {

	Demo obj;

	std::cout << "enter value of x : " << std::endl;
	std::cin >> obj;

	obj.getData();
	return 0;
}
