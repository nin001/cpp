// Constructor
//
// Invoked when object is created , used to initialize instance varible
//3 types
//  no argument constructor
//  parameterized constructor
//  copy constructor

#include <iostream>

class Demo {
	int x;
	public:
		Demo() {
			std::cout << "no arg constructor" << std::endl;
		}

		Demo(int x) {
			std::cout << "para constructor" << std::endl;
		}

		Demo(Demo &xyz) {
			std::cout << "Copy constructor" << std::endl;
		}
};

int main() {

	Demo obj;	//no-arg
	
	Demo obj2(10);	//para 

	Demo obj3(obj);		//copy
	Demo obj4 = obj;	//copy
	return 0;
}
