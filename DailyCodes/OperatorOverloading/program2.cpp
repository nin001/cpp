// Operator overloading can be achived using three ways
//
// - Using friend function
// - Using normal function
// - Using member function

// Using friend function 
// Overloading insertion operator '<<'
// prototype of operator<<
//
// ostream& operator<<(ostream& 'cout' , Demo& 'obj');

#include <iostream>

class Demo {
	int x = 10;
	public:
	Demo(int x) {
		this->x = x;
	}
	//friend function operator<<
	friend std::ostream& operator<<(std::ostream& cout , Demo& obj) {
		std::cout << obj.x ;
		return cout;			//cout is returned here so that it can then go with further inserton operation
	}
};

int main() {
	Demo obj(100);

	std::cout << obj << std::endl;

	return 0;
}
