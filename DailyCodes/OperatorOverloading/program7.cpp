// overloading operator + using member function

#include <iostream>

class Demo {
	int x = 10;
	public:
	Demo(int x) {
		this->x = x;
		std::cout << "in constructor" << std::endl;
	}

	int operator+(Demo& obj2) {			//First paramter is the hidden this which will map with obj1
		return this->x + obj2.x;
	}
};

int main() {

	Demo obj1(1000);
	Demo obj2(5000);

	std::cout << obj1+obj2 << std::endl;

	return 0;
}
