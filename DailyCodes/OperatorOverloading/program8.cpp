// Operator - overloading
// using normal

#include <iostream>

class Demo {
	int x = 100;

	public :
	Demo(int x ) {
		this->x = x;
	}

	int getX() const{
		return x;
	}

};

int operator-(const Demo& obj1 ,const Demo& obj2){

	return obj1.getX() - obj2.getX();
}
int main() {

	Demo obj1(1500);
	Demo obj2(1000);

	std::cout << obj1-obj2 << std::endl;
	return 0;
}


