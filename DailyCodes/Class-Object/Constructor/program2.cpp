// Constructor as setter(para)
// 
// Copy constructor is used to notify that our objects
// replication is done 

#include <iostream>

class Demo {
	int x;
	int y;

	public:
		Demo() {
			std::cout << "no-args" << std::endl;
		}

		Demo(int x , int y) {
			this->x = x;
			this->y = y;
			std::cout << "para as setter" << std::endl;
		}

		Demo(Demo &obj) {
			std::cout << "copy" << std::endl;
		}

		void disp() {
			std::cout << "x - "<< this->x << std::endl;
			std::cout << "y - "<< this->y << std::endl;
		}
};

int main() {

	Demo obj(100,200);

	obj.disp();

	return 0;
}
