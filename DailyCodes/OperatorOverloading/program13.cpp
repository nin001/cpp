// overloading operator / using normal function

#include <iostream>

class Demo {
	int x = 10;
	public:
	Demo(int x) {
		this->x = x;
		std::cout << "in comstructor" << std::endl;
	}

	int getX() const {
		return x;
	}
};

int operator/(const Demo& obj1 , const Demo& obj2) {
	return obj1.getX()/obj2.getX();
}

int main() {

	Demo obj1(150);
	Demo obj2(150);

	std::cout << obj1/obj2 << std::endl;
	return 0;
}
