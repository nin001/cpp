// Extraction operator overloading '>>'

// Overloading using friend function

#include <iostream>

class Demo {
	int x;
	public:
	friend std::istream& operator>>(std::istream& in , Demo& obj) {
		in>>obj.x;
		return in;
	}

	void getData() {
		std::cout << "value of x is : " << this->x << std::endl;
	}
};


int main() {

	Demo obj1;

	std::cout << "Enter x value in the object : ";
	std::cin >> obj1;

	obj1.getData();
	return 0;
}
