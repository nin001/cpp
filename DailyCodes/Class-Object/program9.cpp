// Passing default argumenets in cpp

#include <iostream>

class Demo {
	int x = 10;
	int y = 20;

	public:

	Demo() {
		std::cout << "no-arg constructor" << std::endl;
	}


	// if user created object with just one value then it will be problamatic
	// Demo(10) -> no constructor for this type of object creation
	// To overcome this we can use default arguments in cpp 
	//
	//
	// Default arguments in cpp starts to read from the left 
	// so default arguments should be present at the last part
/*	
	void fun(int x = 100 , int y){

	}		// default argument missing for parameter 2
*/

	// we can also write constructor with default arguments
	Demo(int x , int y = 200) {
		this->x = x;
		this->y = y;
		std::cout << "para constructor with default arguments" << std::endl;
	}

	// if we write constructor with both default value it is similar to no-arg constructor
	// so there could be ambigious constructor
/*	
	Demo(int x = 400 , int y = 200) {
		this->x = x;
		this->y = y;
		std::cout << "para constructor with default arguments" << std::endl;
	}
*/		
	
};

int main() {

	Demo obj;

	Demo obj2(10,20);

	Demo obj3(20);
	return 0;
}
