// Temp object gets created when we call constructor inside a constructor
// which gets destroyed (calls destructor) after scope

#include <iostream>

class Demo {
	public:
		int x = 10;
		Demo() {
			std::cout << "in constructor" << std::endl;
			std::cout << x << std::endl;
		}

		Demo(int x) {
			this->x = x;
			std::cout << "in constructor para" << std::endl;
			std::cout << x << std::endl;
			Demo();
		}

		~Demo() {
			std::cout << "destructor" << std::endl;
		}
};

int main() {

	Demo obj(50);
	std::cout << "end main" << std::endl;
	return 0;
}
