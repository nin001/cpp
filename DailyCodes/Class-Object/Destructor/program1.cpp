// Destructor is a method which is called when the objects is killed or freed the memory
// Destructor is called to notify that the object is killed and to free up the resources
// in the Destructor method so that it can be used by other entities
//
// Destructor is implecitly called when we create an normal object (without new) , it is
// called when the scope of the object is over
// But objects created with new that is malloc , gets memory in the heap which is global 
// for everyone , and after scope is over of that object , no implecit call to the destructor
// is made , we have to call it explicitly

#include <iostream>

class Demo {
	int x = 10;

	public:
	Demo() {
		std::cout << "in constructor" << std::endl;
	}

	~Demo() {
		std::cout << "in destructor" << std::endl;

	}
};

int main() {

	Demo obj1;
	Demo *obj2 = new Demo();	//To call the destructor of objects created with new , 
					//we need to free the object with the help of 
					//delete keyword which internally calls the free() method
	delete obj2;
	//delete obj1;		cannot delete the objects present in the stack frame , needs pointer like obj2

	return 0;
}
