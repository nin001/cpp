// classes and objects
//
// Class is a blueprint which consists its own 
// features and functionality , through which Objects
// are created and functionality is used 
//
// Class is considered as UserDefined Data type that is the
// reason that class block ends with semi colon
//
// Class is Blueprint that means Class doesnt get memory , 
// Classes object gets memory
// 
// Objects are of two types , 1 - Normal object , object created
// in the stack frame 
// 2 - Object created with new keyword that is created in the heap
// section
// Normal object is like normal variables , 
// Objects created with new are stored in heap and their address is
// stored under the pointer which is in the stack frame

// class is made up of 
// 	variables
// 		-static
// 		-non static
// 	functions (member functions)
// 		-static
// 		-non static
// 		-constructor
// 			-copy constructor
// 		-destructor
//
// BY default class members are private

#include <iostream>

class Demo {
	public:
		//static variable
		//static int x = 10; static vairables cannot be used directly in cpp 
		// We can use static variables in the class 
		// 	By making it constant
		// 	By Declaring staic variable in the class
		// 	and then initializing outside the class

		static const int s1 = 100;
		static int s2;
		//non static variable
		int y = 20;

		// constructor
		Demo() {
			std::cout << "In constructor" << std::endl;
		}

		// member functions
		void fun() {
			std::cout << " non-static fun" << std::endl;
		}

		static void gun() {
			std::cout << "static fun" << std::endl;
		}

		//copy constructor

		Demo(Demo& obj) {
			std::cout << "Copy constructor" << std::endl;
		}

		//Destructor
		~Demo() {
			std::cout << "In Destructor" << std::endl;
		}
};

int Demo::s2 = 500;

int main() {

	Demo *obj = new Demo();
//function call
	obj->fun();

//static members call
// Use of Scope resolution to access static members
	Demo::gun();
	std::cout << Demo::s1 << std::endl;
	std::cout << Demo::s2 << std::endl;
//copy constructor

	Demo obj2 = *obj;

//destructor call
	delete obj;
	
	return 0;
}
