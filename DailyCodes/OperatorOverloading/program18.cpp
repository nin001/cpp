// overloading delete operator in cpp

#include <iostream>

class Demo {
	int x;
	public:
	Demo(int x) {
		this->x = x;
	}

	void operator delete(void* ptr) {
		std::cout << "in delete overloading function" << std::endl;
		free(ptr);
	}
};

int main() {

	Demo *obj = new Demo(100);

	delete obj;
	return 0;
}
