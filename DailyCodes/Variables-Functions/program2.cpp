// reference variable

#include <iostream>

int main() {
	int x = 10;

	int &y = x;

	std::cout << &x << std::endl;
	std::cout << &y << std::endl;
	std::cout << x << std::endl;
	std::cout << y << std::endl;
	
	// giving address of reference variable and changing the data of x
	int *ptr = &y;

	*ptr = 20;
	
	std::cout << &x << std::endl;
	std::cout << &y << std::endl;
	std::cout << ptr << std::endl;
	std::cout << *ptr << std::endl;

	// changing value of x using reference variable
	
	y = 30;
	std::cout << x << std::endl;

	// Tring changing the reference of the y to other variable
	/*
	int z = 2000;
	&y = z;

	std::cout << z << std::endl;
	std::cout << y << std::endl;
	std::cout << &y << std::endl;
	std::cout << &z << std::endl;
*/
	//cannot change the reference variable reference to other variable of same type(data)

}
