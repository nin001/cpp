// returning reference variable which is a local variable
//
// which means returning the address of a variable which is local
// whoms stack frame can be popped while accessing 
//
#include <iostream>

int& fun(int x) {
	int y = x+50;


	return y;
}

int main() {
	int x = 10;

	int ret = fun(x);

	std::cout << ret << std::endl;
}

/*
 * program10.cpp: In function ‘int& fun(int)’:
program10.cpp:12:16: warning: reference to local variable ‘y’ returned [-Wreturn-local-addr]
   12 |         return y;
      |                ^
program10.cpp:9:13: note: declared here
    9 |         int y = x+50;
      |             ^
*/
