// ambiguity in the functions while reference variable parameters and
// int
#include <iostream>

void fun(int x , int y) {

	std::cout << "IN normal function" << std::endl;
	std::cout << x << y << std::endl;
}

void fun(int &x , int &y) {
	std::cout << "IN reference variable function" << std::endl;
	std::cout << x << y << std::endl;
}

int main() {
	int x = 10;
	int y = 20;

	fun(x,y);
}

/*program9.cpp: In function ‘int main()’:
program9.cpp:20:12: error: call of overloaded ‘fun(int&, int&)’ is ambiguous
   20 |         fun(x,y);
      |         ~~~^~~~~
program9.cpp:5:6: note: candidate: ‘void fun(int, int)’
    5 | void fun(int x , int y) {
      |      ^~~
program9.cpp:11:6: note: candidate: ‘void fun(int&, int&)’
   11 | void fun(int &x , int &y) {
      |      ^~~
*/
