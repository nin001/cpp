// Structure Reference
// Accessing Object of structure through structure reference

#include <iostream>

struct info {
	int data;
};

int main() {

	info obj = {10};
	info &y = obj;

	std::cout << &obj << std::endl;
	std::cout << &y << std::endl;

	return 0;
}
