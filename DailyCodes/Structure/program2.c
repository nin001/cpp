// Structure in c
// WAP to make a nested structure and take user input

#include<stdio.h>
#include<string.h>

struct Player {
	char name[20];
	int jrNo;
};

struct IPL {
	struct Player obj;
	int noOfTeams;
};

void main() {

	struct IPL csk;
	printf("Enter Captain name : ");
	fgets(csk.obj.name,20,stdin);

	// removing \n from string
	int size = strlen(csk.obj.name);
	csk.obj.name[size-1] = '\0';

	printf("Enter Captains jrno : ");
	scanf("%d",&csk.obj.jrNo);

	printf("Enter number of teams : ");
	scanf("%d",&csk.noOfTeams);
	
	printf("Captain name : %s\nJersy Number : %d\nNumber of teams : %d\n",csk.obj.name,csk.obj.jrNo,csk.noOfTeams);

}
