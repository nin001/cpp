// using namespace std
// standard namespace is namespace whose identifiers we cannot use 
// identifiers outside (in our program) are in global namespace

#include<iostream>

using namespace std;

int main(){
	int cout = 10;

	std::cout<<cout<<std::endl;
}

//output using namespace std
//program3.cpp: In function ‘int main()’:
//program3.cpp:12:19: error: invalid operands of types ‘int’ and ‘<unresolved overloaded function type>’ to binary ‘operator<<’
//   12 |         cout<<cout<<endl;
//      |         ~~~~~~~~~~^~~~~~

//output without using namespace std and using "::" operator
//10

// output when using namespace and and "::" operator as well
// 10
