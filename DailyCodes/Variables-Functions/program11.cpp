// Variables in cpp
// are same as c
// cpp has added new initialization techniques for variables
// 
// - copy initialization = int x = 10  (also called as normal initialization)
// - direct initialization = int x(10) 
// - uniform initialization = int x{10} also called as braces initialization
// 					and list initialization

#include <iostream>

int main() {

	int x = 10;	//normal/copy
	
	int y(100); 	// direct init
	
	int z{20};	// uniform/braces/list init
	

	std::cout << x << " " << y << " " << z << std::endl;
	return 0;
}
