// call by reference 
//
// (it is same as call by address)

#include<iostream>

void fun(int &y , int &x) {

	std::cout << x << y << std::endl;
}

int main() {
	int x = 10;
	int y = 20;

	fun(x,y);
}

/*
 INTERNALLY
 parameter and arguments are as : 
 int &y = x;
 which is internally --->  int *const y = &x;

 int &x = y;
 which is internally --> int*const x = &y;
 */
