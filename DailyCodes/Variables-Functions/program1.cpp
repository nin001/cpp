// Variables initialization in cpp
// Types :
// 1 . Direct Initialization
// 2 . Uniform Initialization/List initialization/Braces Initialization
// 3 . Copy Initialization

#include <iostream>

int main(){
	int a = 10; //- Copy initialization
	
	int b(20);  //- Direct initialization
	
	int c{30};  //- Uniform initialization
	
	std::cout << a << std::endl;
	std::cout << b << std::endl;
	std::cout << c << std::endl;
}
