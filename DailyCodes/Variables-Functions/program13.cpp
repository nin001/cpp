// name mangling
// 
// C doesnt allow to overload a function ., 
// but in cpp overloading is achived using name mangling
// name of the functions and function called is changed according
// to the paramter list
// eg : there are two overloaded functions
// 
// int add (int, int) --> name magled : addii(int , int)
// int add(int , float) --> name mangled : addif(int , float)

#include <iostream>

int add(int a , int b) {
	return a+b;
}

int add(int a , int b , int c) {
	return a+b+c;
}

int main() {

	std::cout << add(10,20) << std::endl;
	std::cout << add(10,20,30) << std::endl;
	return 0;
}
