// Overloading operator +

#include <iostream>

class Demo {
	int x = 10;

	public:
	Demo(int x) {
		this->x = x;
		std::cout << "in constructor" << std::endl;
	}

	friend int operator+(Demo& obj1 , Demo& obj2);
};

int operator+(Demo& obj1 , Demo& obj2) {
	return obj1.x + obj2.x;
}

int main() {

	Demo obj1(300);
	Demo obj2(100);

	std::cout << obj1+obj2 << std::endl;
	return 0;
}
