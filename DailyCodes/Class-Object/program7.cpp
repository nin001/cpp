// static variables and static member functions in class

#include <iostream>

class Demo {

	public:
	int x = 10;   //instance variable or non-static variable
	
//	static int y = 10;
	static int y;    // In cpp static variables declaration is allowed inside the class but we cannot
			 // initialize the static/class variable inside the class 
			 // Init/Assignment of static variable is done outside the class in global namespace 
			 // using scope resolution operator
};

int Demo::y = 30;

int main() {

	Demo obj;
	std::cout << obj.x << std::endl;

	std::cout << Demo::y << std::endl;	//class variables can be accessed using class name without creating
						//object as static variables are present inside data section
						//and common for all variables and initialized before main

	
	return 0;
}
/*
program7.cpp:10:20: error: ISO C++ forbids in-class initialization of non-const static member ‘Demo::y’
   10 |         static int y = 10;
         |*/
