// Inline function
//
// Inline function is a function which is declared inline
// which is a request to compiler that to not push stack frame of 
// the function and dump the functions code where it is called
//

#include<iostream>

inline int add(int x , int y) {
	return x+y;
}

int main() {
	int sum = add(10,20);
}
