// Object creation in cpp
//
// Object in Cpp can be created using 2 ways
// 1. <Class Name> identifier;
// crates object in the stack frame 
// 2. using new keyword
// creates object in heap section and returns pointer
// to the identifier

#include <iostream>

class Player {
	int jrNo = 18;
	std::string name = "Virat";

	public:
	void info() {
		std::cout << "Name : " << name << std::endl;
		std::cout << "Jr no : " << jrNo << std::endl;
	}
};

int main() {

	Player vk;
	vk.info();
	return 0;
}
