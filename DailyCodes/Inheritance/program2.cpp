// Changing data of Employee object using setter and getter function

#include <iostream>

class Employee {
	std::string eName = "Niraj";
	int empId = 101;

	public:
		void getData() {
			std::cout << eName << " " << empId << std::endl;
		}

		void setter(std::string eName , int empId) {
			this->eName = eName;
			this->empId = empId;
		}
};

class Company {
	std::string cname = "Veritas";
	int eCount = 1500;
	Employee obj;

	public:
	Company(std::string cname , int eCount , std::string eName , int empId) {
		this->cname = cname;
		this->eCount = eCount;
		obj.setter(eName,empId);
	}

	void getInfo() {
		std::cout << cname << " " << eCount << std::endl;
		obj.getData();
	}
};

int main() {
	Company obj("Pubmatic",5000,"Atharva",123);

	obj.getInfo();
	return 0;
}
