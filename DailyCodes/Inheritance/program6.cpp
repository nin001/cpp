// Internal calls for  object that is created
// with new and object that is created in stack frame(normal)

#include <iostream>

class Parent {
	public:
		Parent() {
			std::cout << "Parent constructor" << std::endl;
		}
		~Parent() {
			std::cout << "Parent Destructor" << std::endl;
		}
};

class Child : public Parent {
	public:
		Child() {
			std::cout << "Child constructor" << std::endl;
		}
		~Child() {
			std::cout << "Child destructor" << std::endl;
		}
		friend void* operator new(size_t size) {
			std::cout << "Child new " << std::endl;
			return malloc(size);
		}

		void operator delete(void* ptr) {
			std::cout << "Child delete" << std::endl;
			free(ptr);
		}
};


int main() {

	//Child obj1;	//Intenrally 
	//Child()	//Direct constructor call is placed
	
	Child *obj2 = new Child();
	// objects that are created using new 
	// 
	// **obj2 is a pointer which is present in main stack frame
	// **and object is created in heap section
	//
	// 1) operator new(sizeof(Child)); --> internally first
	// 	operator new is called which allocates memory to the object and 
	// 	returns the pointer
	// ***opeator new : void* operator new(size_t size);
	// 2) address that is returned using operator new is copied in obj2
	// 3) Child class constructor is called 
	//    Child(obj); --> obj(address) -->this
	
	delete obj2;
	// 1) Destructor of obj2 is called to notifiy
	// 2) operator delete is called in which memory of object is freed
	// **operator delete : void operator delete(void*);

	return 0;
}
