// Operator overloading need
//
// When we define a class and try to print the class
// i.e. std::cout << obj << std::endl;
//
// Error of no matching function found for operator<< 
// opeator insertion
//
// Points to remember
// * When we do any operator tasks such as +,-,*,/,%,new,delete etc
// it internally calls a method of opeartor<operator using>
// which is predefined and does the operation for us , 
// * To overcome above error we and overload operator methods and 
// as these methods are not called explicitly , to overload these functions
// is called as operator overloading

#include <iostream>

class Demo {

	public:
		int x = 110;
		int y = 220;
};
int main() {
	Demo obj;

	std::cout << obj << std::endl;

	return 0;
}
