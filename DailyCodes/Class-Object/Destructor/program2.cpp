// delete and delete[] keywords
//
// delete keyword is used to delete and free the space for single objects and
// variabls  , such as pointer to the object , primitive data types variables 
// created with the help of new.
//
// delete[] is used to free up the space of an array , array returns a pointer 
// and if we use delete keyword for free the space of array , it just frees the poiter
// not the array with multiple elements , so delete[] is used to free up array , 
// which free up each element of the array i.e. it clears(deletes) each element
// present in the array

#include <iostream>

class Demo {
	int *ptr = new int();		//ptr = 8bytes -> storing address of int in heap which is 4 bytes
	int *arrptr = new int[50];	//arrptr = 8bytes -> storing address of the first element of the array
					//array size = 200 bytes 
	public:
	Demo() {
		std::cout << "in costructor" << std::endl;
	}

	~Demo() {
		delete ptr;	//frees 4 bytes in the heap
		delete[] arrptr;	// frees 200 bytes array in the heap
		std::cout << "in destructor" << std::endl;
	}
};

int main() {

	Demo *obj = new Demo();

	delete obj;
	return 0;
}
