// object creation using new

#include <iostream>

class Demo {
	int x;

	public:
	void setData(int data) {
		x = data;
	}

	void info() {
		std::cout << x << std::endl;
	}
};

int main() {

	Demo *obj = new Demo();

	std::cout << "Enter a value : " ;
	int x;
	std::cin >> x;
	obj->setData(x);

	obj->info();

	return 0;
}
