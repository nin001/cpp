// Classes and Objects
//
// Access specifiers
// 3 Access specifiers :
// 1.Public
// 2.Private
// 3.Protected
// 
// By default in class without any access specifier 
// they are private

#include <iostream>

class Demo {
	int x = 10;	//by default private
	
	protected:
	int y = 20;

	public:
	int z = 30;
};


int main() {

	Demo obj;
	std::cout << obj.x << std::endl;
	std::cout << obj.y << std::endl;
	std::cout << obj.z << std::endl;
	return 0;	
}
/*
program3.cpp: In function ‘int main()’:
program3.cpp:28:26: error: ‘int Demo::x’ is private within this context
   28 |         std::cout << obj.x << std::endl;
      |                          ^
program3.cpp:15:13: note: declared private here
   15 |         int x = 10;     //by default private
      |             ^
program3.cpp:29:26: error: ‘int Demo::y’ is protected within this context
   29 |         std::cout << obj.y << std::endl;
      |                          ^
program3.cpp:18:13: note: declared protected here
   18 |         int y = 20;
      |*/
