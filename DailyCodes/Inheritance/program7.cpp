// Parameterized constructor
// initializing instance variable of Parent 

#include <iostream>

class Parent {
	std::string str;
	int x;
	public:
		Parent(std::string str , int x) {
			std::cout << "Parent constructor para" << std::endl;
			std::cout << this << std::endl;
			this->str = str;
			this->x = x;
		}
		void getData() {
			std::cout << str << std::endl;
			std::cout << x << std::endl;
		}

};

class Child : public Parent {
	int z;
	public:
		Child(std::string str , int x , int z):Parent(str,x) {
			std::cout << "Child constructor para" << std::endl;
			std::cout << this << std::endl;
			this->z = z;
		}

		void  printData() {
			std::cout << z << std::endl;
		}
};
		

int main() {
	Child obj("Niraj",50,60);

	obj.getData();
	obj.printData();
	
	return 0;
}
