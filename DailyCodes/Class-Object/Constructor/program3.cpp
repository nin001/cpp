// Copy constructor invokation scenarios 

#include <iostream>

class Demo {
	int x = 10;
	int y = 20;

	public:
		Demo() {
			std::cout << "no arg contructor" << std::endl;
		}

		Demo(int x , int y) {
			std::cout << "para constructor" << std::endl;
		}

		Demo(Demo & obj ) {
			std::cout << "copy constructor" << std::endl;
		}
};

int main() {
	Demo obj1;

	Demo obj2(10,20);
	
	obj1 = obj2;
	Demo obj3 = obj1;
	Demo obj4(obj1);

	return 0;
}
