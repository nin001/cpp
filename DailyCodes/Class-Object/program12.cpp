// Constant object in cpp

#include <iostream>

class Demo {
	public:
		int x = 10;
		Demo() {
			this->x = 50;
			std::cout << "in constructor" << std::endl;
		}

		void getData() const {
			std::cout << x << std::endl;
		}
};

int main() {

	const Demo obj;

	obj.getData();
	return 0;
}
