// Overloading relational operators 
// such as , 
// <= , >= , == , != 
//
// This operator also call their operator functions that can
// be overloaded because , the user defined class cannot be
// compared or related by using their functions

#include <iostream>

class Demo {
	int x = 10;
	public:
	Demo(int x) {
		this->x = x;
	}

	bool operator==(Demo& obj) {
		if(this->x == obj.x) 
			return true;
		else
			return false;
	}
};


int main() {

	Demo obj1(500);
	Demo obj2(600);
	Demo obj3(500);

	if(obj1 == obj2) {
		std::cout << "obj1 is equal to obj2" << std::endl;
	}else {
		std::cout << "obj1 is not equal to obj2" << std::endl;
	}

	if(obj1 == obj3) {
		std::cout << "obj1 is equal to obj3" << std::endl;
	}else {
		std::cout << "obj1 is not equal to obj3" << std::endl;
	}
	
	return 0;
}
