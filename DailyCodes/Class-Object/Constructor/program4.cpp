// As a new replication of the object is created
// then after changing the instance variable of one object
// will not affect the change in the instance variable of the 
// other object whose replication is made
// or vice versa

#include <iostream>

class Demo {
	public:
	int x = 10;

	public:
		Demo() {
			std::cout << "no - arg constructor" << std::endl;
		}

		Demo( Demo &obj) {
			std::cout << "copy constructor" << std::endl;
		}
};

int main() {
	
	Demo obj1;

	Demo obj2 = obj1;

	std::cout << obj1.x << std::endl;
	std::cout << obj2.x << std:: endl;

	obj2.x = 100;

	std::cout << obj1.x << std::endl;
	std::cout << obj2.x << std:: endl;

	return 0;
}
