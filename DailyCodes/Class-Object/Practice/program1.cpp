// First point to learn in classes and object is
// the Structure
//
// Structure in cpp is different than of c
//
// Structure is used to various data types under
// single name, cpp had added functionality 
// - Can assign values to the variables in the structure
// - can write functions in the structure and can access the varibales
// in the function
// - in c we needed to use struct <structure name> obj to create object
// but in cpp Direct name of the structure is used to create object

#include <iostream>

struct Player {

	/*private*/ int jrNo;
	std::string name;
	// if we use char[] in player , we need to compulsory give size to it even if we assign variable value
	

	void disp() {
		std::cout << "Name : " << name << std::endl;
		std::cout << "jrNo : " << jrNo << std::endl;
	}
};

/**** By default functions and variables inside the structure is public
 * Template is present in the data section , but object of the structure
   gets memory not the structuer
 * Structure functions and variables can also have access specifiers , 
 * public , private 

*/

int main() {

	Player obj = {18,"Virat"};

	//obj.jrNo = 18;
	//obj.name = "Virat";

	std::cout << obj.jrNo << std::endl;
	std::cout << obj.name << std::endl;

	obj.disp();

	return 0;
}
