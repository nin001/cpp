// Using normal function
//
// as normal function cannot access the private variables a method is needed to be
// definded in the class that returns the variable values so that it can be printed
// using operator overloading

#include <iostream>

class Demo {

	int x = 10;
	public:
	int getX() {
		return x;
	}
};

// normal function overloading operator<<
std::ostream& operator<<(std::ostream& cout , Demo& obj ) {
	std::cout << obj.getX() ;
	return cout;				// ALWAYS RETURN COUT FOR METHOD CHAINING 
}

int main() {

	Demo obj;

	std::cout << obj << std::endl;
	return 0;

}
