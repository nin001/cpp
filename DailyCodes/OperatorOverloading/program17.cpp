// Overloading new and delete operator
// new and delete operator can be overloaded

#include <iostream>

class Demo {
	int x = 10;
	public:
	Demo(int x) {
		this->x = x;
	}

	void fun() {
		std::cout << "in fun" << std::endl;
	}
};

void* operator new(size_t size) {

	std::cout << "overloading new operator" << std::endl;
	void* ptr = malloc(size);
	return ptr;
}

int main() {

	Demo *obj = new Demo(150);
	return 0;
}
