// First program in Cpp
// This is the first program in cpp
// cpp versions -- 2002 2005 2008 2011 2014 2017 2020 2023

#include<iostream>

int main(){		// in cpp entry point functions need to be of return type of int 
	std::cout<<"Welcome to cpp"<<std::endl;
	
	return 0;     // successful return
}

//Points to remember
//1.include file in cpp is not of header file 
//--reason because header file only consist declarations but in iostream there exist classes defined and as well as namespaces
//
//2.namespace -- use of identifier names some of them are used by standard namespace 
//		which we cannot use if we use the std namespace but can use if we specify the standard
//		namespace by using "::" operator 
//3.return type of main is int other wise there will be errors
//4.Compilation process is same as c compilation
//  -Preprocessing - code of iostream is dumped above out code
//  -compilation - Code is coverted to assembly code 
//  -assembling - Assembly code is converted to the object file (binary)
//  -linking - predefined function(lib) code is linked to our code using libc++ file 

