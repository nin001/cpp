// overloading operator + using normal function

#include <iostream>

class Demo {
	int x = 10;
	public:
	Demo(int x) {
		this->x = x;
		std::cout << "in constructor" << std::endl;
	}

	int getX() {
		return this->x;
	}
};

int operator+(Demo& obj1 , Demo& obj2) {
	return obj1.getX() + obj2.getX();
}

int main() {

	Demo obj1(100);
	Demo obj2(300);

	std::cout << obj1+obj2 << std::endl;
	return 0;
}
