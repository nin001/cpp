//array
//
//Passing 1d 2d array to method

#include <iostream>

//1d array
void arrayCall(int *arr , int size) {
	for(int i = 0 ; i<size ; i++) {
		*(arr+i) = *(arr+i) + 10;
	}
}

//2d array

void arrayCall(int (*arr)[3] , int row , int col) {

	for(int i = 0 ; i<row ; i++) {
		for(int j = 0 ; j<col ; j++) {
			*(*(arr+i)+j) += 100;
		}
	}
}

int main() {

	//1d array
	int arr[] = {10,20,30,40,50};

	arrayCall(arr,sizeof(arr)/sizeof(arr[0]));

	int size = sizeof(arr)/sizeof(arr[0]);
	for(int i = 0 ; i<size ; i++) {
		std::cout << *(arr+i) << " " ;
	}
	std::cout << std::endl;

	//2-d array
	int multArr[][3] = {{1,2,3},{4,5,6},{7,8,9}};

	int row = 3;
	int col = 3;
	
	arrayCall(multArr,row,col);

	for(int i = 0 ; i<row ; i++) {
		for(int j = 0 ; j<col ; j++) {
			std::cout << *(*(multArr+i)+j) << " ";
		}
		std::cout << std::endl;
	}

	return 0;
}
