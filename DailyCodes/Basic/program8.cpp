// cout  and other standard space 
// objects identifier as our variables

#include <iostream>

//using namespace std;
// if we use namespace std in our code
// and give our variables their objects identifiers
// then our code wont work , and to achive our code to work
// we need to give std scope resolution to the object

int main() {

	int cout = 0;

	std::cout << "Value : " << cout <<endl;
	return 0;
}
