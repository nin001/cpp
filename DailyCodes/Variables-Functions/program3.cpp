// Functions in cpp
// header or prototypes
// returntype identifier(variable1 data type , variable2 data type , ..);


// macro functions

#include<iostream>
#define sum(x,y) x+y


int main() {
	std::cout << sum(10,20) << std::endl;
}

/*
# 12 "program3.cpp"
int main() {
 std::cout << 10 +20 << std::endl;
}		^ 
		during preprocessing step the sum(10,20) is replaces to the macro function
		that is 10+20

		sum() stack frame will be not pushed
*/
