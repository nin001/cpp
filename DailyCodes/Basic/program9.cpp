// Cpp has three types of calling functions in cpp
// - call by value ; in this function call we pass the
// value of the element that is copied in the variables of 
// the function , in call by value , value is changed only
// in the function stack frame .
//
// - call by address ; in this function call , address of 
// variable is passed to the function , a pointer of respective
// data type stores the address of the variable whose address is 
// being passed , changes made in function stack frame affects
// the value in the caller function stack frame 
// (values are changed accros stack frame)
//
// - call by reference : call by reference is , in a function 
// a variable is passed and paramter is reference type of that variable
// this means the the reference variable is the variable that is 
// passed in the function call , 
// Reference is like giving a variable allices
//
// But internally , reference varibale is pointer
// Compiler internally changes the statement in the following way
//  int x = 10;
//  int &y = x;
//  here compiler changes the statement as 
//  int *y = &x;
//  and while accessing the values using reference variables , compiler 
//  implicitly dereferences the reference variable for us
//  that is 
//  std :: cout << y << std::endl;
//  internally compiler changes
//  std::cout << *y << std::endl;


//call by value

#include <iostream>

void funValue(int x) {
	std::cout << "Call by value" << std::endl;

}

void fun(int *x) {
	std::cout << "call by address" << std::endl;
}

void fun(int &x) {
	std::cout << "call by reference" << std::endl;
}


int main() {

	int x = 10;

	funValue(x);			//call by value
	fun(&x);		//call by address
	fun(x);		//call by  reference

	/* If we overload call by value and call by reference
	 * then function will be ambigious*/

	return 0;
}
