// operator - overloading using member function

#include <iostream>

class Demo {
	int x = 10;
	public:
	Demo(int x) {
		this->x = x;
	}

	int operator-(const Demo& obj) {
		return this->x - obj.x;
	}
};

int main() {
	
	Demo obj1(18);
	Demo obj2(8);

	std::cout << obj1-obj2 << std::endl;

	return 0;

}
