// Call by reference


// In call by reference function call matches
// eg
// void fun(int &a)    --> invokation fun(x);
//
// above statement changes as int &a = x; ==> int *a = &x;

#include <iostream>

class Demo {

	public:
	std::string str = "Niraj";

};

void fun(int &x) {
	std::cout << x << std::endl;
}

void fun(Demo &obj) {
	std::cout << "Class function called" << std::endl;
	
	std::string str;
	std::cout << "Enter string : ";
	std::cin >> str;

	obj.str = str;
}

int main() {

	//fun(10);	//cannot bind non-constant lvalue to rvalue int
	
	Demo *obj = new Demo();
	fun(*obj);
	std::cout << obj->str << std::endl;
	return 0;
	
}
